package com.example.searchrestapi.service;

import com.example.searchrestapi.entity.Product;
import com.example.searchrestapi.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    @Override
    public List<Product> searchProducts(String query) {
        return productRepository.searchProduct(query);
    }
    @Override
    public Product createProduct(Product product){
        return productRepository.save(product);
    }
}
